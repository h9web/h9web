<?php
ob_start();
session_start();

require_once __DIR__ . '/_app/Config.inc.php';

?><!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="UTF-8">
            <meta name="mit" content="2017-09-18T18:53:00-03:00+17397">
            <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=0">
            <title>Webjump - Desafio</title>                  
        </head>
        <body>        
            <?php 
            // 
            if (file_exists(REQUIRE_PATH . "/dashboard.php")):
                require REQUIRE_PATH . "/dashboard.php";
            else:
                trigger_error('Crie um arquivo /dashboard.php na pasta do tema!');
            endif;
            ?>
        </body>
    </html>
<?php

ob_end_flush();

if (!file_exists('.htaccess')):
    $htaccesswrite = "RewriteEngine On\r\nOptions All -Indexes\r\n\r\n# WC WWW Redirect.\r\n#RewriteCond %{HTTP_HOST} !^www\. [NC]\r\n#RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]\r\n\r\n# WC HTTPS Redirect\r\nRewriteCond %{HTTP:X-Forwarded-Proto} !https\r\nRewriteCond %{HTTPS} off\r\nRewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]\r\n\r\n# WC URL Rewrite\r\nRewriteCond %{SCRIPT_FILENAME} !-f\r\nRewriteCond %{SCRIPT_FILENAME} !-d\r\nRewriteRule ^(.*)$ index.php?url=$1";
    $htaccess = fopen('.htaccess', "w");
    fwrite($htaccess, str_replace("'", '"', $htaccesswrite));
    fclose($htaccess);
endif;
?>