# Sistema desenvolvido como avaliação de conhecimentos em PHP, para empresa Webjump

# Estrutura e organização do código e dos arquivos
- Utilizado o padrão MVC para a estrutura de arquivos e classes.
- Aceso inicial ao sistema através do arquivo index.php na raiz.

# Requisitos
- Banco de Dados MySQL(MariaDB) 10.4.6;
- Servidor Apache 2.4.39;
- PHP versão 7.3.8;
- Servidor local pacote Xampp 3.2.4

# Para rodar o projeto:
- Importar a base de dados desafio.sql;
- Copie o diretório assessment-backend-xp para um diretório local (htdocs - xammp);
- Com o servidor local rodando, acesse o diretóri assessment-backend-xp via browser;
- Alterar parametros de url e base de dados no arquivo _app/Config.inc.php