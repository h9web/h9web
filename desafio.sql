-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 17-Maio-2020 às 13:18
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `desafio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ws_products`
--

CREATE TABLE `ws_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_price` varchar(50) DEFAULT NULL,
  `product_description` text DEFAULT NULL,
  `product_qtde` int(11) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ws_products`
--

INSERT INTO `ws_products` (`product_id`, `product_name`, `product_code`, `product_price`, `product_description`, `product_qtde`, `product_category`) VALUES
(1, 'Tênis Nike Masculino', 'SKU-001', 'R$340', 'Ideal para caminhadas.', 5, 'Tênis Masculino'),
(2, 'Tênis Puma Sports', 'SKU-OO2', 'R$ 280,00', 'Tênis unisex para caminhada.', 5, 'Tênis Masculino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ws_products_categories`
--

CREATE TABLE `ws_products_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ws_products_categories`
--

INSERT INTO `ws_products_categories` (`category_id`, `category_name`, `category_code`) VALUES
(1, 'Tênis Masculino', 'GOI-002'),
(2, 'Tênis Masculino', 'GOI-002'),
(3, 'GOI-003', 'Tênis Feminino'),
(4, 'Tênis Infantil', 'G00-004');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `ws_products`
--
ALTER TABLE `ws_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Índices para tabela `ws_products_categories`
--
ALTER TABLE `ws_products_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `ws_products`
--
ALTER TABLE `ws_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `ws_products_categories`
--
ALTER TABLE `ws_products_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
