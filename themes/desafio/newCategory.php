<?php
require_once __DIR__ . '../../../_app/Config.inc.php';
// AUTO INSTANCE OBJECT READ
if (empty($Read)) :
  $Read = new Read;
endif;

// AUTO INSTANCE OBJECT CREATE
if (empty($Create)) :
  $Create = new Create;
endif;

//CADASTRAR CATEGORIA
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$categoryName = $PostData['category_name'];
$categoryCode = $PostData['category_code'];

$category = ['category_name' => $categoryName, 'category_code' => $categoryCode];
$Create->ExeCreate(DB_PDT_CATS,  $category);

if (!empty($Create->getResult())) :
  echo "<h1>CATEGORIA CRIADA COM SUCESSO!</h1>";
  header('Location: https://localhost/webjump/assessment-backend-xp/themes/desafio/categories.php&id=' . $Create->getResult());
  exit;
else :
  echo "<h1>OPPS! ALGUM PROBLEMA OCORREU!</h1>";
endif;

//EDITAR CATEGORIA

$CategoryId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
if ($CategoryId) :
  $CategoryUpdate = [
    "category_name" => $Category['category_name'],
    "category_code" => $Category['category_code']
  ];
  $Update->ExeUpdate(DB_PDT_CATS, $CategoryUpdate, "WHERE category_id = :id", "id={$CategoryId}");

  if ($Update->getResult()) :
    echo "<h1>Foram atualizado(s) {$Update->getRowCount()}</h1>";
  endif;
  header('Location: https://localhost/webjump/assessment-backend-xp/themes/desafio/categories.php&id=' . $Create->getResult());
endif;
