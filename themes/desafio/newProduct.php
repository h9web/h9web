<?php 
 require_once __DIR__ . '../../../_app/Config.inc.php';
  // AUTO INSTANCE OBJECT READ
  if (empty($Read)):
      $Read = new Read;
  endif;

  // AUTO INSTANCE OBJECT CREATE
  if (empty($Create)):
      $Create = new Create;
  endif;

$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$product = [
    'product_name'        => $PostData['product_name'],
    'product_code'        => $PostData['product_code'],
    'product_price'       => $PostData['product_price'],
    'product_description' => $PostData['product_description'],
    'product_qtde'        => $PostData['product_qtde'],
    'product_category'    => $PostData['product_category']
];
  
  $Create->ExeCreate(DB_PDT,  $product);

  if($Create->getResult()):
    echo "<h1>PRODUTO CADASTRADO COM SUCESSO!</h1>";
    header('Location: https://localhost/webjump/assessment-backend-xp/themes/desafio/products.php&id=' . $Create->getResult());
    exit;
  else:
    echo "<h1>OPPS! ALGUM PROBLEMA OCORREU!</h1>";
  endif;

 
?>